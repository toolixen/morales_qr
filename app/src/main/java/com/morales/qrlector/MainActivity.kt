package com.morales.qrlector

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.hardware.Camera
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.util.DisplayMetrics
import android.util.Log
import android.webkit.URLUtil
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.vision.MultiProcessor
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import com.morales.qrlector.lectorQR.BarcodeTracker
import com.morales.qrlector.lectorQR.BarcodeTrackerFactory
import com.morales.qrlector.lectorQR.CameraSource
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException

class MainActivity : AppCompatActivity(), BarcodeTracker.BarcodeGraphicTrackerCallback {

    private var cameraSource: CameraSource? = null

    val TAG = "Barcode-reader"
    val RC_HANDLE_CAMERA_PERM = 2
    private val RC_HANDLE_GMS = 9001

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        if (rc == PackageManager.PERMISSION_GRANTED) {
            cameraSource = createCameraSource(true, false)
        } else {
            requestCameraPermission()
        }
    }

    override fun onDetectedQrCode(barcode: Barcode?) {
        if (barcode != null) {

            if ( ! URLUtil.isValidUrl(barcode.displayValue) ) {
                Snackbar.make(window.decorView.rootView, "El QR no contiene una URL valida.", Snackbar.LENGTH_SHORT).show()
                return
            }

            Snackbar.make(window.decorView.rootView, "Abriendo: ${barcode.displayValue}", Snackbar.LENGTH_SHORT).show()

            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(barcode.displayValue))
            startActivity(browserIntent)

            finish()
        }
    }

    private fun requestCameraPermission() {
        val permissions = arrayOf(Manifest.permission.CAMERA)
        ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM)
    }

    override fun onRequestPermissionsResult(requestCode: Int,permissions: Array<String>, grantResults: IntArray) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            Log.d(TAG, "Got unexpected permission result: " + requestCode)
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            return
        }
        resultCameraPermission(requestCode, grantResults)
    }

    fun resultCameraPermission(requestCode: Int, grantResults: IntArray) {
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            createCameraSource(true, false)
            return
        }

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Se necesita acceso a camara")
                .setMessage("Esta aplicación no se puede ejecutar porque se concedio permiso de usar cámara. La aplicación se cerrara.")
                .setPositiveButton("Ok") { _, _ -> finish() }
                .show()
    }

    fun createCameraSource(autoFocus: Boolean, useFlash: Boolean): CameraSource? {
        val barcodeDetector = BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.ALL_FORMATS)
                .build()
        val barcodeFactory = BarcodeTrackerFactory(this)
        barcodeDetector.setProcessor(MultiProcessor.Builder(barcodeFactory).build())

        if (!barcodeDetector.isOperational) {
            val lowstorageFilter = IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW)
            val hasLowStorage = this.registerReceiver(null, lowstorageFilter) != null

            if (hasLowStorage) {
                Log.w(TAG, "Face detector dependencies cannot be downloaded due to low device storage")
            }
        }
        val metrics = DisplayMetrics()
        this.windowManager.defaultDisplay.getMetrics(metrics)

        var builder: CameraSource.Builder = CameraSource.Builder(this, barcodeDetector)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(metrics.widthPixels, metrics.heightPixels)
                .setRequestedFps(24.0f)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            builder = builder.setFocusMode(
                    if (autoFocus) Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE else null)
        }

        return builder
                .setFlashMode(if (useFlash) Camera.Parameters.FLASH_MODE_TORCH else null)
                .build()
    }


    @SuppressLint("MissingPermission")
    fun startCameraSource() {
        val code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this)
        if (code != ConnectionResult.SUCCESS) {
            val dlg = GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS)
            dlg.show()
        }

        if (cameraSource != null) {
            try {
                cameraPreview.start(cameraSource)
            } catch (e: IOException) {
                Log.e(TAG, "Unable to start camera source.", e)
                cameraSource!!.release()
                cameraSource = null
            }

        }
    }

    override fun onResume() {
        super.onResume()
        startCameraSource()
    }

    override fun onPause() {
        super.onPause()
        cameraPreview.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraPreview.release()
    }

}
